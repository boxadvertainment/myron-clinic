<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id = DB::table('pages')->insertGetId([
            'title' => 'Accueil',
            'slug' => '/',
            'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dapibus pretium mi, eget dignissim neque sollicitudin sit amet. Ut rhoncus elit non lobortis pulvinar. Suspendisse facilisis condimentum dolor a placerat. Aenean ut tincidunt nunc, non molestie dui. Aenean euismod leo sapien, at convallis nisi tempor imperdiet. Phasellus blandit nibh a metus porttitor mattis. Sed finibus tellus id enim feugiat, nec tincidunt turpis finibus. Donec tincidunt mi nec diam vulputate bibendum. Donec congue consectetur ipsum, a sodales enim malesuada id. Mauris et nulla ante. Ut quis consequat nibh, vel gravida ex. Mauris purus tortor, lobortis in sollicitudin a, eleifend sit amet turpis. Nunc in ornare sem, vitae ullamcorper nibh.',
            'locale' => 'fr',
            'created_at' => new DateTime()
        ]);
        DB::table('pages')->insert([
            'title' => 'Home',
            'slug' => '/',
            'content' => 'Mauris cursus ante odio, nec placerat massa mollis dignissim. Aliquam erat volutpat. Aliquam vel commodo neque. Etiam et erat velit. Donec ac libero odio. Fusce sed metus vitae massa dictum pellentesque. Curabitur aliquam placerat nisi non ultrices. Donec euismod mauris in rhoncus eleifend. Proin sollicitudin ligula quis felis ornare iaculis. Proin id mi in velit condimentum sodales eget ut sem. Proin feugiat sem in libero pharetra, a iaculis mi porta.',
            'locale' => 'en',
            'created_at' => new DateTime(),
            'page_id' => $id
        ]);

        $id = DB::table('pages')->insertGetId([
            'title' => 'A propos',
            'slug' => 'a-propos',
            'content' => 'Duis id mollis enim. In in nulla ex. Nunc id lacus dictum, congue justo ut, eleifend ex. Phasellus luctus risus sit amet elementum accumsan. Pellentesque ac dolor consequat dolor laoreet congue. Nunc eu mauris eget lectus malesuada semper nec sed velit. Nulla in justo quis lorem consequat malesuada et a velit. Sed id vulputate velit.',
            'locale' => 'fr',
            'created_at' => new DateTime()
        ]);
        DB::table('pages')->insert([
            'title' => 'About',
            'slug' => 'about',
            'content' => 'Etiam vulputate rutrum enim, at dapibus urna porta eu. Vestibulum in arcu quis ex aliquam venenatis non id libero. Phasellus consectetur volutpat nibh a gravida. Nunc posuere nunc mauris, eget cursus nisi maximus non. Donec ante nisi, pellentesque quis lorem in, scelerisque ultricies mauris. Vestibulum rhoncus nisl sit amet tincidunt egestas. Mauris ac finibus risus. Donec et facilisis quam, consectetur facilisis ipsum.',
            'locale' => 'en',
            'created_at' => new DateTime(),
            'page_id' => $id
        ]);
    }
}
