var gulp     = require('gulp');
var Elixir   = require('laravel-elixir');
var dotenv   = require('dotenv').load();
var _        = require('underscore');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');

var Task     = Elixir.Task;
var config   = Elixir.config;

Elixir.extend('images', function(options) {
    config.img = {
        folder: 'img',
        outputFolder: 'img'
    };

    options = _.extend({
        progressive: true,
        interlaced : true,
        svgoPlugins: [{removeViewBox: false, cleanupIDs: false}],
        use: [pngquant()]
    }, options);

    var paths = new Elixir.GulpPaths()
        .src(config.get('assets.img.folder'))
        .output(config.get('public.img.outputFolder'));

    new Task('imagemin', function () {
        return gulp.src(paths.src.path)
            .pipe(imagemin(options))
            .on('error', function(e) {
                new Elixir.Notification().error(e, 'Minifying Images Failed!');
                this.emit('end');
            })
            .pipe(gulp.dest(paths.output.path));
    })
        .watch(paths.src.baseDir + '/**/**');
});
