<!-- Main Menu -->
<nav class="navbar">
    <div class="container">
        <div class="navbar-header col-md-3  col-xs-12">
            <div class="fake-back left"></div>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="{{ asset('img/myron-logo.png') }}" alt="Myron Clinic"></a>
        </div>
        <div class="navbar-wrapper col-md-9 pull-right col-xs-12">
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    @include(config('laravel-menu.views.bootstrap-items'), array('items' => $mainNav->roots()))
                    <li><a href="{{url('cafeteria')}}"><i class="fa fa-user"></i> Cafeteria </a></li>
                    <li><a href="{{url('clinique')}}"><i class="fa fa-user"></i> Clinique </a></li>
                    <li><a href="{{url('chirugie')}}"><i class="fa fa-user"></i> Chirugie </a></li>
                    <li><a href="{{url('hotel')}}"><i class="fa fa-user"></i> Hotel </a></li>
                    <li><a href="#" class="highlighted"><i class="fa fa-user"></i> Espace patient </a></li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown</a>
                      <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Action</a></li>
                      </ul>
                    </li>
                </ul>

                {{--<ul class="nav navbar-nav navbar-right">--}}
                {{--<li class="dropdown">--}}
                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ app()->getLocale() }} <span class="caret"></span></a>--}}
                {{--{!! $langMenu->asUl( array('class' => 'dropdown-menu') ) !!}--}}
                {{--</li>--}}
                {{--</ul>--}}
            </div><!--/.nav-collapse -->
            <div class="fake-back right"></div>
        </div>

    </div><!--/.container -->
</nav>