@extends('layout')

@section('class', 'cafeteria')

@section('content')

    <div class="bannerwithtitle clearfix">
        <img src="img/bg.jpg" alt="banner" class="img-responsive">
        <h1 class="page-title">LA CLINIQUE</h1>
    </div>

    <div class="clearfix"></div>

    <div class="clinique-container">

        <div class="container">

                <h2 class="lined">A propos de nous</h2>
            <br>

                <div class="row">
                    <div class="col-lg-6">
<!--
<iframe width="300" height="240" src="https://www.youtube.com/embed/E-8X37wXDu0" frameborder="0">
</iframe>
-->
                       <!--
                        <video id="video-player" class="video-js vjs-default-skin vjs-big-play-centered" width="350px" height="240px"
                               controls preload="none" data-setup='{ "aspectRatio":"350:240", "playbackRates": [1, 1.5, 2] }' data-src="http://solutions.brightcove.com/bcls/assets/videos/Bird_Titmouse.mp4" data-type="video/mp4">
                        </video>
                        -->

                    </div>
                    <div class="col-lg-6">
                        <h2 class="right-title">la clinique myron</h2>
                        <p>Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression.</p>
                        <p>Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. </p>
                        <p>Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. </p>
                        <p>Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.</p>
                    </div>
                </div>

            <br>
        </div>

        <div class="row">

            <div class="col-lg-7 bg1">
                <div class="padding-left">
                    <h2 class="right-title">la clinique myron</h2>
                    <p>Le Lorem Ipsum est simplement du faux texte.</p>
                    <p>Le Lorem Ipsum est simplement du faux texte.</p>
                    <p>Le Lorem Ipsum est simplement du faux texte.</p>
                    <p>Le Lorem Ipsum est simplement du faux texte.</p>
                    <p>Le Lorem Ipsum est simplement du faux texte.</p>
                    <p>Le Lorem Ipsum est simplement du faux texte.</p>
                    <p>Le Lorem Ipsum est simplement du faux texte.</p>
                    <p>Le Lorem Ipsum est simplement du faux texte.</p>
                </div>
            </div>
            <div class="col-lg-5 bg2">
                <div class="center_block">
                    <h4>Suivez nous sur</h4>
                    <div class="socialmedia">
                        <a href="" class="social"><i class="fa fa-facebook"></i></a>
                        <a href="" class="social"><i class="fa fa-twitter"></i></a>
                        <a href="" class="social"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div>
            </div>

        </div>

        <div class="four-column">

            <div class="container">

                <div class="col-lg-3 col-md-6">
                    <h2 class="title">130</h2>
                    <h3 class="sub-title">Lorem Ipsum</h3>
                    <p class="description">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet</p>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h2 class="title">24</h2>
                    <h3 class="sub-title">Lorem Ipsum</h3>
                    <p class="description">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet</p>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h2 class="title">47</h2>
                    <h3 class="sub-title">Lorem Ipsum</h3>
                    <p class="description">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet</p>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h2 class="title">07</h2>
                    <h3 class="sub-title">Lorem Ipsum</h3>
                    <p class="description">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet</p>
                </div>

            </div>

        </div>

        <div class="container">
            <hr class="separator-block">
        </div>

        <div class="gallerie">

            <div class="container">

                <i class="fa fa-picture-o fa-4x" aria-hidden="true"></i>
                <h2>GALLERIE PHOTO</h2>
                <p class="text-center">Le Lorem Ipsum est simplement du faux texte employé dans la composition.</p>

                <div class="padding"></div>

                <div class="row">
                    <div class="col-lg-3 col-md-4"><img class="image-border-radius" src="img/chirurgie-generale.jpg"></div>
                    <div class="col-lg-3 col-md-4"><img class="image-border-radius" src="img/chirurgie-generale.jpg"></div>
                    <div class="col-lg-3 col-md-4"><img class="image-border-radius" src="img/chirurgie-generale.jpg"></div>
                    <div class="col-lg-3 col-md-4"><img class="image-border-radius" src="img/chirurgie-generale.jpg"></div>
                </div>

                <div class="row">
                    <div class="col-lg-3 col-md-4"><img class="image-border-radius" src="img/chirurgie-generale.jpg"></div>
                    <div class="col-lg-3 col-md-4"><img class="image-border-radius" src="img/chirurgie-generale.jpg"></div>
                    <div class="col-lg-3 col-md-4"><img class="image-border-radius" src="img/chirurgie-generale.jpg"></div>
                    <div class="col-lg-3 col-md-4"><img class="image-border-radius" src="img/chirurgie-generale.jpg"></div>
                </div>

            </div>
        </div>


    </div>

@endsection