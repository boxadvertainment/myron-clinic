@extends('layout')

@section('class', 'cafeteria')

@section('content')

    <div class="banner-image clearfix">
        <img src="img/bg.jpg" alt="banner">
    </div>

    <div class="text">
        <div class="container">

            <h2>CAFETERIA</h2>
            <p>On sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même. L'avantage du Lorem Ipsum sur un texte générique comme 'Du texte. Du texte. Du texte.' est qu'il possède une distribution de lettres plus ou moins normale, et en tout cas comparable avec celle du français standard. De nombreuses suites logicielles de mise en page ou éditeurs de sites Web ont fait du Lorem Ipsum leur faux texte par défaut, et une recherche pour 'Lorem Ipsum' vous conduira vers de nombreux sites qui n'en sont encore qu'à leur phase de construction. Plusieurs versions sont apparues avec le temps, parfois par accident, souvent intentionnellement (histoire d'y rajouter de petits clins d'oeil, voire des phrases embarassantes).</p>

        </div>
    </div>

    <div class="gallerie">

        <div class="container">

            <h2>GALLERIE PHOTO</h2>
            <p>Le Lorem Ipsum est simplement du faux texte employé dans la composition.</p>

            <div class="row">

                <div class="col-lg-3 col-md-4"><img class="image-border-radius" src="img/chirurgie-generale.jpg"></div>
                <div class="col-lg-3 col-md-4"><img class="image-border-radius" src="img/chirurgie-generale.jpg"></div>
                <div class="col-lg-3 col-md-4"><img class="image-border-radius" src="img/chirurgie-generale.jpg"></div>
                <div class="col-lg-3 col-md-4"><img class="image-border-radius" src="img/chirurgie-generale.jpg"></div>

            </div>


            <div class="row">

                <div class="col-lg-3 col-md-4"><img class="image-border-radius" src="img/chirurgie-generale.jpg"></div>
                <div class="col-lg-3 col-md-4"><img class="image-border-radius"  src="img/chirurgie-generale.jpg"></div>
                <div class="col-lg-3 col-md-4"><img class="image-border-radius" src="img/chirurgie-generale.jpg"></div>
                <div class="col-lg-3 col-md-4"><img class="image-border-radius" src="img/chirurgie-generale.jpg"></div>

            </div>

        </div>
    </div>

@endsection