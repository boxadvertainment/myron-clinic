@extends('layout')

@section('class', 'Chirugie')

@section('content')

    <div class="bannerwithtitle clearfix">
        <img src="img/bg.jpg" alt="banner" class="img-responsive">
        <h1 class="page-title">Orthpédique Orthpédique Orthpédique</h1>
    </div>

    <div class="clearfix"></div>

    <section class="content-wrapper">
        <div class="container">

            <h2>Définition</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam consequatur cumque, debitis, delectus et expedita harum ipsa ipsum magnam necessitatibus sunt totam veritatis? Dolore eaque fuga incidunt maxime provident quidem quos vero. Accusamus aliquid autem dignissimos eaque maiores, molestias nesciunt placeat! Autem consectetur consequatur expedita illum labore, repellat ut! Pariatur!</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam consequatur cumque, debitis, delectus et expedita harum ipsa ipsum magnam necessitatibus sunt totam veritatis? Dolore eaque fuga incidunt maxime provident quidem quos vero. Accusamus aliquid autem dignissimos eaque maiores, molestias nesciunt placeat! Autem consectetur consequatur expedita illum labore, repellat ut! Pariatur!</p>

            <h2>AVANT L’INTERVENTION</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam consequatur cumque, debitis, delectus et expedita harum ipsa ipsum magnam necessitatibus sunt totam veritatis? Dolore eaque fuga incidunt maxime provident quidem quos vero. Accusamus aliquid autem dignissimos eaque maiores, molestias nesciunt placeat! Autem consectetur consequatur expedita illum labore, repellat ut! Pariatur!</p>

            <div class="row">
                <div class="col-md-4">
                    <img src="img/chirurgie-generale.jpg" class="img-responsive">
                </div>
                <div class="col-md-8">
                    <p>On sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même.</p>
                    <p>L'avantage du Lorem Ipsum sur un texte générique comme 'Du texte. Du texte. Du texte.' est qu'il possède une distribution de lettres plus ou moins normale, et en tout cas comparable avec celle du français standard.</p>
                    <p>De nombreuses suites logicielles de mise en page ou éditeurs de sites Web ont fait du Lorem Ipsum leur faux texte par défaut, et une recherche pour 'Lorem Ipsum' vous conduira vers de nombreux sites qui n'en sont encore qu'à leur phase de construction.</p>
                    <p>Plusieurs versions sont apparues avec le temps, parfois par accident, souvent intentionnellement (histoire d'y rajouter de petits clins d'oeil, voire des phrases embarassantes).</p>
                </div>
            </div>

            <h2>AVANT L’INTERVENTION</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Aliquam consequatur cumque, debitis, delectus et expedita harum ipsa ipsum magnam necessitatibus sunt totam veritatis? Dolore eaque fuga incidunt maxime provident quidem quos vero.</p>
            <p>Accusamus aliquid autem dignissimos eaque maiores, molestias nesciunt placeat! Autem consectetur consequatur expedita illum labore, repellat ut! Pariatur!</p>

            <p class="btn-plusinfo">
                <a href="#">Demandez plus d'informations</a>
            </p>

            <br><br>

            <div class="owl-carousel">
                <div class="item">
                    <div class="leftPart"><img src="{{ asset('img/img1.jpg') }}"></div>
                    <div class="rightPart">
                        <p class="title-item">Chirugie maxillo-facial esthétique, plastique et réparatrice</p>
                        <p class="description-item">Spécialité chirugicale regroupant l'ensemble des intervention consistant...</p>
                        <a href="#" class="btnSuite">lire la suite</a>
                    </div>
                </div>
                <div class="item">
                    <div class="leftPart"><img src="{{ asset('img/img1.jpg') }}"></div>
                    <div class="rightPart">
                        <p class="title-item">Chirugie maxillo-facial esthétique, plastique et réparatrice</p>
                        <p class="description-item">Spécialité chirugicale regroupant l'ensemble des intervention consistant...</p>
                        <a href="#" class="btnSuite">lire la suite</a>
                    </div>
                </div>
                <div class="item">
                    <div class="leftPart"><img src="{{ asset('img/img1.jpg') }}"></div>
                    <div class="rightPart">
                        <p class="title-item">Chirugie maxillo-facial esthétique, plastique et réparatrice</p>
                        <p class="description-item">Spécialité chirugicale regroupant l'ensemble des intervention consistant...</p>
                        <a href="#" class="btnSuite">lire la suite</a>
                    </div>
                </div>
            </div>

        </div>

    </section>

@endsection