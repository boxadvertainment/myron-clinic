@extends('layout')

@section('class', 'home')

@section('content')

    <section id="banner" class="clearfix">
        <div class="col-md-7 image-block">
            <img src="{{ asset('img/banner.jpg') }}">
        </div>
        <div class="col-md-5 text-block">
            <h2 class="title"><small>Chirurgie </small> Esthétique <div class="title-line"></div></h2>
            <p class="des">La chirurgie pédiatrique est une branche de la achirurgie qui se limite à une tranche d'âge s'étendant de la période prénatale à l'adolescence</p>
            <a href="#" class="btn-link read-more"> Lire la suite </a> <a href="#" class="fa fa-facebook"></a> <a href="#" class="fa fa-twitter"></a>
            <a href="#" class="get-appointment-block">
                <img src="{{ asset('img/get-appointment-block-img.png') }}" alt="">
            </a>
        </div>
    </section>

    <section class="prez">

        <div class="container">
            <h2 class="section-title">Nos prestations <div class="title-line center"></div></h2>
            <p>
                Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte.</p>
        </div>


        <div class="container">

            <div class="owl-carousel">

                <div class="item">
                    <div class="leftPart"><img src="{{ asset('img/img1.jpg') }}"></div>
                    <div class="rightPart">
                        <p class="title-item">Chirugie maxillo-facial esthétique, plastique et réparatrice</p>
                        <p class="description-item">Spécialité chirugicale regroupant l'ensemble des intervention consistant...</p>
                        <a href="#" class="btnSuite">lire la suite</a>
                    </div>
                </div>
                <div class="item">
                    <div class="leftPart"><img src="{{ asset('img/img1.jpg') }}"></div>
                    <div class="rightPart">
                        <p class="title-item">Chirugie maxillo-facial esthétique, plastique et réparatrice</p>
                        <p class="description-item">Spécialité chirugicale regroupant l'ensemble des intervention consistant...</p>
                        <a href="#" class="btnSuite">lire la suite</a>
                    </div>
                </div>
                <div class="item">
                    <div class="leftPart"><img src="{{ asset('img/img1.jpg') }}"></div>
                    <div class="rightPart">
                        <p class="title-item">Chirugie maxillo-facial esthétique, plastique et réparatrice</p>
                        <p class="description-item">Spécialité chirugicale regroupant l'ensemble des intervention consistant...</p>
                        <a href="#" class="btnSuite">lire la suite</a>
                    </div>
                </div>
                <div class="item">
                    <div class="leftPart"><img src="{{ asset('img/img1.jpg') }}"></div>
                    <div class="rightPart">
                        <p class="title-item">Chirugie maxillo-facial esthétique, plastique et réparatrice</p>
                        <p class="description-item">Spécialité chirugicale regroupant l'ensemble des intervention consistant...</p>
                        <a href="#" class="btnSuite">lire la suite</a>
                    </div>
                </div>

            </div>

        </div>

    </section>

    <section class="apropos">

        <div class="container">

            <h2 class="section-title">A PROPOS DE NOUS <div class="title-line center"></div></h2>
            <p>créée en 2015 la clinique MYRON vous accueille au coeur de quertier résidentiel du lac II a Tunis.</p>
            <p>Dans une ambiance raffinée et luxueuse, un personnel compétent et accueillant est a votre dispositon pour répondre a vos attentes et questions durant votre traitement et hospitalisation.</p>
            <p>L'établissement assure un service de proximité, tant en hospitalisation compléte qu'en prise en chrge en ambulatoire.</p>
            <p>Centre médicochirugical pluridisciplinaire offrant des equipements ultramodernes et fonctionnels, la clinique Myron bénéficie d'un plateau de consultations médicales et chirugicales ainsi que d'un laboratoire d'analyses médicales et radiologique sur place.</p>
            <div class="suitelink"><a href="">Lire La Suite</a></div>
            <div class="socialmedia">
                <a href="" class="social"><i class="fa fa-facebook"></i></a>
                <a href="" class="social"><i class="fa fa-twitter"></i></a>
                <a href="" class="social"><i class="fa fa-linkedin"></i></a>
            </div>

        </div>

    </section>

    <section class="specialite">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="{{ asset('img/doctor.png') }}" alt="">
                </div>
                <div class="col-md-6">
                    <h2 class="section-title">A PROPOS DE NOUS <div class="title-line"></div></h2>
                    <p>crée en 2015 la clinique MYRON vous accueille au coeur de quertier résidentiel du lac II a Tunis.</p>
                    <p>Dans une ambiance raffinée et luxueuse, un personnel compétent et accueillant est a votre dispositon pour répondre a vos attentes et questions durant votre traitement et hospitalisation.</p>
                    <p>L'établissement assure un service de proximité, tant en hospitalisation compléte qu'en prise en chrge en ambulatoire.</p>
                    <p>Centre médicochirugical pluridisciplinaire offrant des equipements ultramodernes et fonctionnels, la clinique Myron bénéficie d'un plateau de consultations médicales et chirugicales ainsi que d'un laboratoire d'analyses médicales et radiologique sur place.</p>
                    <a href="#" class="link">Link Link Link</a>
                </div>
            </div>
        </div>
    </section>

    <section class="testimonial">

        <div class="container">


            <div class="testimonials-carousel">

                <div class="temoignage">
                    <ul class="ultem">
                        <li class="quoteleft"><i class="fa fa-quote-left" aria-hidden="true"></i></li>
                        <li class="title"><h2>Témoignages</h2></li>
                        <li class="quoteright"><i class="fa fa-quote-right" aria-hidden="true"></i></li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="content">
                        <img src="{{ asset('img/img1.jpg') }}">
                        <p>Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression.</p>
                        <div class="nameuser">Foulen ben foulena <br> (patiente)</div>
                    </div>
                </div>

                <div class="temoignage">
                    <ul class="ultem">
                        <li class="quoteleft"><i class="fa fa-quote-left" aria-hidden="true"></i></li>
                        <li class="title"><h2 >Temoignages</h2></li>
                        <li class="quoteright"><i class="fa fa-quote-right" aria-hidden="true"></i></li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="content">
                        <img src="{{ asset('img/img1.jpg') }}">
                        <p>Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression.</p>
                        <div class="nameuser">Foulen ben foulena <br> (patiente)</div>
                    </div>
                </div>

            </div>

        </div>

    </section>

    <section class="map">
        <div class="map-canvas" id="map-canvas"></div>
    </section>


@endsection

@section('scripts')
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript">
        window.onload = function(){

            var config = {
                latitude  : 48.865460,
                longitude : 2.321120,
                location  : 'Paris, Ile de France, France'
            };

            // Création d'un objet pLatLng pour stocker les coordonnées
            var latlng = new google.maps.LatLng(config.latitude, config.longitude);

            // Options de la carte
            var myOptions = {
                zoom: 2,
                center: latlng,
                // mapTypeId: google.maps.MapTypeId.SATELLITE
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            // Création et affichage de la carte dans le div map-canvas
            var map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);


            var image = {
                // Adresse de l'icône personnalisée
                url: 'img/map-marker.png',
                // Taille de l'icône personnalisée
                size: new google.maps.Size(25, 40),
                // Origine de l'image, souvent (0, 0)
                origin: new google.maps.Point(0,0),
                // L'ancre de l'image. Correspond au point de l'image que l'on raccroche à la carte. Par exemple, si votre îcone est un drapeau, cela correspond à son mâts
                anchor: new google.maps.Point(0, 20)
            };

            // Ajout d'un marqueur sur la carte
            var mark = new google.maps.Marker({
                position: latlng,
                map:      map,
                title:    config.location,
                icon: image.url
            });
        }
    </script>
@endsection
