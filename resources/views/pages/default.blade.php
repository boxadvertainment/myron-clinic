@extends('layout')

@section('class', $page->type.' '.$page->slug)

@section('content')

        <header class="header" style="background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 50%,rgba(0,0,0,0.6) 100%), url( {{ asset('img/'. $page->slug .'.jpg') }} );">
                <div class="container">
                        <h1 class="page-title">{{ $page->title }}</h1>
                </div>
        </header>

        <section class="content-wrapper">
                <div class="container">
                        <h2>Définition</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam consequatur cumque, debitis, delectus et expedita harum ipsa ipsum magnam necessitatibus sunt totam veritatis? Dolore eaque fuga incidunt maxime provident quidem quos vero. Accusamus aliquid autem dignissimos eaque maiores, molestias nesciunt placeat! Autem consectetur consequatur expedita illum labore, repellat ut! Pariatur!</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam consequatur cumque, debitis, delectus et expedita harum ipsa ipsum magnam necessitatibus sunt totam veritatis? Dolore eaque fuga incidunt maxime provident quidem quos vero. Accusamus aliquid autem dignissimos eaque maiores, molestias nesciunt placeat! Autem consectetur consequatur expedita illum labore, repellat ut! Pariatur!</p>

                        <h2>AVANT L’INTERVENTION</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam consequatur cumque, debitis, delectus et expedita harum ipsa ipsum magnam necessitatibus sunt totam veritatis? Dolore eaque fuga incidunt maxime provident quidem quos vero. Accusamus aliquid autem dignissimos eaque maiores, molestias nesciunt placeat! Autem consectetur consequatur expedita illum labore, repellat ut! Pariatur!</p>
                        {!! $page->content !!}
                </div>
        </section>

@endsection
