@extends('layout')

@section('class', $page->type.' home')

@section('content')

    <section id="banner" class="clearfix">
        <div class="col-sm-5 col-md-7 image-block">
            <img src="{{ asset('img/banner.jpg') }}">
        </div>
        <div class="col-sm-7 col-md-5 text-block">
            <h2 class="title"><small>Chirurgie </small> Esthétique <div class="title-line"></div></h2>
            <p class="des">La chirurgie pédiatrique est une branche de la achirurgie qui se limite à une tranche d'âge s'étendant de la période prénatale à l'adolescence</p>
            <a href="#" class="btn-link read-more"> Lire la suite </a> <a href="#" class="fa fa-facebook"></a> <a href="#" class="fa fa-twitter"></a>
            <a href="#" class="get-appointment-block">
                <img src="{{ asset('img/get-appointment-block-img.png') }}" alt="">
            </a>
        </div>
    </section>

    {{-- <h1>{{ $page->title }}</h1> --}}

    <!-- Page Content -->

    {!! $page->content !!}

    <!-- Page Content -->

@endsection

@section('scripts')
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:15,
                nav:false,
                dots:true,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                    },
                    1000: {
                        items: 3
                    }
                }
            });

            var config = {
                latitude  : 36.844045,
                longitude : 10.282969,
                location  : 'Lac II, Tunis, Tunisie'
            };

            // Création d'un objet pLatLng pour stocker les coordonnées
            var latlng = new google.maps.LatLng(config.latitude, config.longitude);

            // Options de la carte
            var myOptions = {
                zoom: 15,
                center: latlng,
                scrollwheel: false,
                // mapTypeId: google.maps.MapTypeId.SATELLITE
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles :[{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"geometry.fill","stylers":[{"visibility":"on"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#b4d4e1"},{"visibility":"on"}]}]
            };

            // Création et affichage de la carte dans le div map_canvas
            var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

            // Ajout d'un marqueur sur la carte
            var mark = new google.maps.Marker({
                position: latlng,
                map:      map,
                title:    config.location
            });
        });
    </script>
@endsection
