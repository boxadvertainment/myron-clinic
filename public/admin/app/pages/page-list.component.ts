import {Component, OnInit, AfterViewInit, ElementRef}   from 'angular2/core';
import {Page, PageService} from './page.service';
import {DataTables} from '../directives/datatables.directive';
import {Router, RouteParams} from 'angular2/router';
//import jQuery = require('jquery');
// import * as $ from "jquery";

declare var $ : any;
@Component({
  templateUrl: './app/pages/page-list.html',
  directives: [DataTables]
})
export class PageListComponent implements OnInit, AfterViewInit {
  public pages: Page[];
  constructor(
    public pageService: PageService,
    public el: ElementRef,
    private _router: Router,
    routeParams: RouteParams) {
  }
  ngOnInit() {
    this.pageService.getPages().subscribe(
      pages => this.pages = pages,
      error => console.log('Could not load todos.')
    );
  }
  ngAfterViewInit(){
    var that = this;
    $(that.el.nativeElement).find('.datatable').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
    });
  }
  onSelect(page: Page) {
    this._router.navigate( ['PageDetail', { id: page.id }] );
  }
}
