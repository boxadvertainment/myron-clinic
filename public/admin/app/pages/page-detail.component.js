System.register(['angular2/core', './page.service', 'angular2/router'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, page_service_1, router_1;
    var PageDetailComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (page_service_1_1) {
                page_service_1 = page_service_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            }],
        execute: function() {
            PageDetailComponent = (function () {
                function PageDetailComponent(_service, _router, _routeParams) {
                    this._service = _service;
                    this._router = _router;
                    this._routeParams = _routeParams;
                }
                PageDetailComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    var id = +this._routeParams.get('id');
                    this._service.getPage(id).subscribe(function (page) {
                        if (page) {
                            //setTimeout(()=> {this.page = page}, 2000);
                            _this.page = page;
                            $('.summernote').summernote();
                        }
                        else {
                            _this.gotoPages();
                        }
                    });
                };
                PageDetailComponent.prototype.ngAfterViewChecked = function () {
                    $('.summernote').summernote();
                };
                PageDetailComponent.prototype.gotoPages = function () {
                    this._router.navigate(['Pages']);
                };
                PageDetailComponent.prototype.save = function () {
                    page = this.page;
                    page.content = $('.summernote').val();
                    this._service.updatePage(this.page).subscribe(function (res) {
                        alert('Enregistrement avec success !!');
                    });
                };
                PageDetailComponent = __decorate([
                    core_1.Component({
                        templateUrl: './app/pages/page-detail.html',
                    }), 
                    __metadata('design:paramtypes', [page_service_1.PageService, router_1.Router, router_1.RouteParams])
                ], PageDetailComponent);
                return PageDetailComponent;
            }());
            exports_1("PageDetailComponent", PageDetailComponent);
        }
    }
});
//# sourceMappingURL=page-detail.component.js.map