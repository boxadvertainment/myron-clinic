import {Component, OnInit}  from 'angular2/core';
import {Page, PageService} from './page.service';
import {RouteParams, Router} from 'angular2/router';

declare var $: any;

@Component({
  templateUrl: './app/pages/page-detail.html',
})
export class PageDetailComponent {
  public page: Page;
  constructor(
    private _service: PageService,
    private _router: Router,
    private _routeParams: RouteParams
    ) { }


  ngOnInit() {
    let id = +this._routeParams.get('id');
    this._service.getPage(id).subscribe(page => {
      if (page) {
          //setTimeout(()=> {this.page = page}, 2000);

        this.page = page;
        $('.summernote').summernote();
      } else { // id not found
        this.gotoPages();
      }
    });
  }

  ngAfterViewChecked() {
      $('.summernote').summernote();
  }

  gotoPages() {
    this._router.navigate(['Pages']);
  }
  save() {
    page = this.page;
    page.content = $('.summernote').val();
    this._service.updatePage(this.page).subscribe(res => {
      alert('Enregistrement avec success !!');
    });
  }
}
