System.register(['angular2/core', './page.service', '../directives/datatables.directive', 'angular2/router'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, page_service_1, datatables_directive_1, router_1;
    var PageListComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (page_service_1_1) {
                page_service_1 = page_service_1_1;
            },
            function (datatables_directive_1_1) {
                datatables_directive_1 = datatables_directive_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            }],
        execute: function() {
            PageListComponent = (function () {
                function PageListComponent(pageService, el, _router, routeParams) {
                    this.pageService = pageService;
                    this.el = el;
                    this._router = _router;
                }
                PageListComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.pageService.getPages().subscribe(function (pages) { return _this.pages = pages; }, function (error) { return console.log('Could not load todos.'); });
                };
                PageListComponent.prototype.ngAfterViewInit = function () {
                    var that = this;
                    $(that.el.nativeElement).find('.datatable').DataTable({
                        "paging": true,
                        "lengthChange": false,
                        "searching": false,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false
                    });
                };
                PageListComponent.prototype.onSelect = function (page) {
                    this._router.navigate(['PageDetail', { id: page.id }]);
                };
                PageListComponent = __decorate([
                    core_1.Component({
                        templateUrl: './app/pages/page-list.html',
                        directives: [datatables_directive_1.DataTables]
                    }), 
                    __metadata('design:paramtypes', [page_service_1.PageService, core_1.ElementRef, router_1.Router, router_1.RouteParams])
                ], PageListComponent);
                return PageListComponent;
            }());
            exports_1("PageListComponent", PageListComponent);
        }
    }
});
//# sourceMappingURL=page-list.component.js.map