<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('test', function () {
    //$page = DB::table('pages as p')->join('pages as pc', 'p.id', '=', 'pc.page_id')->select('p.slug as name', 'pc.slug','pc.title')->where('pc.locale', '=' , 'as')->get()->groupBy('name');
    //dd($page);
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function() {
    /*$locale = Request::segment(3);
    app()->setLocale($locale);
    Route::group(['middleware' => ['api'], 'prefix' => 'api/' . app()->getLocale() ], function(){
        Route::get('pages', 'AdminController@pages');
        Route::get('page/{id}', 'AdminController@page');
    });*/

    Route::group(['middleware' => ['api'], 'prefix' => 'api'], function(){
        Route::resource('page', 'PageController');
    });
    Route::get('{admin}', 'AdminController@index')->where('admin', '.*');
});

Route::group(['middleware' => ['web']], function() {

    $locale = Request::segment(1);
    if (in_array($locale, config('app.available_locales'))) {
        app()->setLocale($locale);
    } else {
        $locale = null;
    }

    Route::get('/', 'AppController@home');
    Route::get('/cafeteria', 'CafeteriaController@home');
    Route::get('/clinique', 'CliniqueController@home');
    Route::get('/hotel', 'HotelController@home');
    Route::get('/chirugie', 'ChirugieController@home');
    Route::get($locale . '/{slug?}', 'AppController@page')->where('slug', '.*');
    Route::get('{locale?}/{slug?}', ['as' => 'page']);
});


