<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Page;

class AppController extends Controller
{
    public function home()
    {
        return view('home');
    }

    public function page($slug = '/')
    {
        $page = Page::ofSlug($slug)->firstOrFail();
        return view('pages.' . $page->type , compact('page'));
    }
}
