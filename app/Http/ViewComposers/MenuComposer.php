<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Page;

class MenuComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $users;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct()
    {
        // Dependencies automatically resolved by service container...
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $query = \DB::table('pages as p')->join('pages as pc', 'p.id', '=', 'pc.page_id')->select('p.slug as name', 'pc.slug','pc.title')->where('pc.locale',  app()->getLocale());
        $nav = \DB::table('pages')->select('slug as name', 'slug','title')->whereNull('page_id')->where('locale',  app()->getLocale())->union($query)->get();
        $nav = array_map(function($item){
            return (array) $item;
        }, $nav);
        $nav = collect($nav)->keyBy('name')->toArray();

        \Menu::make('mainNav', function($menu) use ($nav) {
            $menuItems = ['/', 'a-propos'];
            foreach($menuItems as $item) {
                if ( array_key_exists($item, $nav) ) {
                    if($item == '/'){
                        $menu->add('<i class="fa fa-home"></i>', ['route'  => ['page',
                            'slug' => $nav[$item]['slug'],
                            'locale' => (app()->getLocale() != config('app.default_locale')) ? app()->getLocale() : null
                        ]]);
                    }else {
                        $menu->add($nav[$item]['title'], ['route' => ['page',
                            'slug' => $nav[$item]['slug'],
                            'locale' => (app()->getLocale() != config('app.default_locale')) ? app()->getLocale() : null
                        ]]);
                    }
                }
            }
        });

        $slug = \Request::route()->parameter('slug');
        $page_id = \DB::table('pages')->where('slug', $slug)->value('page_id');
        if ($page_id) {
            $langNav = \DB::table('pages')->select('locale as lang', 'slug')->where('slug', '<>', $slug)->where('page_id', $page_id)->orWhere('id', $page_id)->get();
        } else {
            $langNav = \DB::table('pages as p')->join('pages as pc', 'p.id', '=', 'pc.page_id')->select('pc.locale as lang', 'pc.slug')->where('p.slug', $slug)->get();
        }
        $langNav = array_map(function($item){
            return (array) $item;
        }, $langNav);
        $langNav = collect($langNav)->keyBy('lang')->toArray();

        \Menu::make('langMenu', function($menu) use ($langNav){
            foreach(config('app.available_locales') as $locale) {
                if ($locale != app()->getLocale()) {
                    if (array_key_exists($locale, $langNav)) {
                        $menu->add($locale, ['route'  => ['page',
                            'slug' => $langNav[$locale]['slug'],
                            'locale' => ($langNav[$locale]['lang'] != config('app.default_locale')) ? $langNav[$locale]['lang'] : null]]);
                    } else {
                        $menu->add($locale);
                    }
                }
            }
        });
        //$view->with('menu', $menu);
    }
}